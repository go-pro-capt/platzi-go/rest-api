package server

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/database"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/database/postgres"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/repository"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/server/websockets"
)

type Broker struct {
	router *mux.Router
}

func CreateServer(ctx context.Context, config *database.Config) (*Broker, error) {
	if config.Port == "" {
		return nil, errors.New("port is required")
	}

	if config.JWTSecret == "" {
		return nil, errors.New("secret is required")
	}

	if config.DatabaseUrl == "" {
		return nil, errors.New("DB url is required")
	}

	return &Broker{
		router: mux.NewRouter(),
	}, nil

}

func Start(binder func(r *mux.Router, hub *websockets.Hub)) {
	config := database.BuildConfig()
	server, err := CreateServer(context.Background(), config)

	if err != nil {
		log.Fatal(err.Error())
	}

	SetDbConnection() // Open DB Connection

	// Create a websocket Connection
	hub := websockets.NewHub()
	go hub.Run() // Initialize the Hub

	binder(server.router, hub)
	log.Println("Server is running on Port", config.Port)

	addr := fmt.Sprintf(":%s", config.Port)

	if err := http.ListenAndServe(addr, server.router); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func SetDbConnection() {
	dbconn, _ := postgres.GetConnection()

	if postgres.CheckHealthDbConnection(dbconn) == 0 {
		log.Fatal("No Connection to DB")
		return
	} // Check for a Healthy DB Connection

	err := dbconn.Ping()

	if err != nil {
		log.Fatal(err.Error())
	}

	log.Printf("Open DB Connection, OK ...")

	repository.NewUserRepository(postgres.GetRepo())
	repository.NewPostRepository(postgres.GetRepo())

}
