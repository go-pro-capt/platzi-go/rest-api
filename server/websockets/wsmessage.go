package websockets

import "gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"

type WebsocketMessage struct {
	Type    string       `json:"type"`
	Payload *models.Post `json:"payload"`
}
