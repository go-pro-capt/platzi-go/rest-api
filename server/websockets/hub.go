package websockets

import (
	"encoding/json"
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool { return true },
}

type Hub struct {
	clients    []*Client
	register   chan *Client
	unregister chan *Client
	mutex      *sync.Mutex
}

func NewHub() *Hub {
	return &Hub{
		clients:    make([]*Client, 0),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		mutex:      &sync.Mutex{},
	}
}

func (hub *Hub) Run() {
	for {
		select {
		case client := <-hub.register:
			hub.OnConnect(client)
		case client := <-hub.unregister:
			hub.OnDisconnect(client)
		}
	}
}

func (hub *Hub) OnConnect(client *Client) {
	log.Println("Client Connected: ", client.socket.RemoteAddr())

	hub.mutex.Lock()
	defer hub.mutex.Unlock()

	client.id = client.socket.RemoteAddr().String()
	hub.clients = append(hub.clients, client)

}

func (hub *Hub) OnDisconnect(client *Client) {
	log.Println("Client Disconnected: ", client.socket.RemoteAddr())

	hub.mutex.Lock()
	defer hub.mutex.Unlock()

	i := -1
	for j, c := range hub.clients {
		if c.id == client.id {
			i = j
			break
		}
	}

	copy(hub.clients[i:], hub.clients[i+1:])
	hub.clients[len(hub.clients)-1] = nil
	hub.clients = hub.clients[:len(hub.clients)-1]
}

func (hub *Hub) Broadcast(msg WebsocketMessage, ignore *Client) {
	data, _ := json.Marshal(msg)
	for _, client := range hub.clients {
		if client != ignore {
			client.outbound <- data
		}
	}
}

func (hub *Hub) HandleSocket() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		socket, err := upgrader.Upgrade(w, r, nil)

		if err != nil {
			log.Println(err.Error())
			http.Error(w, "Could not open a Websocket connection", http.StatusBadRequest)
			return
		}

		client := NewClient(hub, socket)
		hub.register <- client

		go client.Write()

	}

}
