package requests

type NewPostRequest struct {
	PostTitle   string `json:"post_title"`
	PostContent string `json:"post_content"`
}

type UpdatePostRequest struct {
	PostTitle   string `json:"post_title"`
	PostContent string `json:"post_content"`
}

type GetAllPostByDateRequest struct {
	Date string `json:"date"`
}
