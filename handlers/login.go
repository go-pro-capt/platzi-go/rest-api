package handlers

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/helpers"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/database"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/repository"
)

func LoginHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var request = SignUpLoginRequest{}
		err := json.NewDecoder(r.Body).Decode(&request)

		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		user, _ := repository.GetUserByFlag(r.Context(), request.Email, "email", "users")

		userM := user.(*models.User)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		if user == nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
		}

		if err := helpers.DecryptPasword([]byte(userM.Password), []byte(request.Password)); err != nil {
			http.Error(w, "Invalid Credentials", http.StatusUnauthorized)
			return
		}

		claims := helpers.AppClaims{
			UserId: userM.Id,
			StandardClaims: jwt.StandardClaims{
				ExpiresAt: time.Now().Add(2 * time.Hour).Unix(),
			},
		}

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		tokenString, err := token.SignedString([]byte(database.BuildConfig().JWTSecret))

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(LoginResponse{
			Token: tokenString,
		})

	}
}
