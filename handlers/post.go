package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/segmentio/ksuid"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/handlers/requests"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/handlers/responses"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/helpers"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/helpers/handlers"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/repository"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/server/websockets"
)

func CreatePostHandler(hub *websockets.Hub) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		authToken, err := helpers.ParseToken(token)

		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			log.Println(err.Error())
		}

		var user_id string

		if claims, ok := authToken.Claims.(*helpers.AppClaims); ok && authToken.Valid {
			user_id = claims.UserId
			if err != nil {
				log.Println(err.Error())
			}
		}

		var request = requests.NewPostRequest{}
		err = json.NewDecoder(r.Body).Decode(&request)

		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		id, err := ksuid.NewRandom()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		var post = models.Post{
			Id:           id.String(),
			Post_Title:   request.PostTitle,
			Post_Content: request.PostContent,
			CreatedAt:    time.Now(),
			UserId:       user_id,
		}

		err = repository.InsertPost(r.Context(), "posts", &post) // DB Transaction

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		var postMessage = websockets.WebsocketMessage{
			Type:    "post_created",
			Payload: &post,
		}

		hub.Broadcast(postMessage, nil)

		log.Printf("New Post has been added, (OK) ...") // New DBTx

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responses.NewPostResponse{
			Msg: "Your post has been added succesfully",
		})
	}
}

func GetAllPostsHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		authToken, err := helpers.ParseToken(token)

		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			log.Println(err.Error())
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responses.AllPostListResponse{
			Posts: handlers.GetAllPosts("all_posts", authToken, r, ""),
		})
	}
}

func GetPostsByUserHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		authToken, err := helpers.ParseToken(token)

		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			log.Println(err.Error())
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responses.AllPostListResponse{
			Posts: handlers.GetAllPosts("by_user", authToken, r, ""),
		})
	}
}

func GetPostsByDateHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		authToken, err := helpers.ParseToken(token)

		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			log.Println(err.Error())
		}

		var request = requests.GetAllPostByDateRequest{}
		err = json.NewDecoder(r.Body).Decode(&request)

		if err != nil {
			log.Println(err.Error())
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responses.AllPostListResponse{
			Posts: handlers.GetAllPosts("by_date", authToken, r, request.Date),
		})
	}
}

func GetPostByIdHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		token := r.Header.Get("Authorization")
		authToken, err := helpers.ParseToken(token)

		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			log.Println(err.Error())
		}

		var post repository.Models
		if authToken.Valid {
			post, err = repository.GetPostBy(r.Context(), params["id"], "id", "posts")
			if err != nil {
				log.Println(err.Error())
			}
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responses.RetrievePostResponse{
			Post: post,
		})
	}
}

func UpdatePostHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		token := r.Header.Get("Authorization")
		authToken, err := helpers.ParseToken(token)

		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			log.Println(err.Error())
		}

		var request = requests.UpdatePostRequest{}
		err = json.NewDecoder(r.Body).Decode(&request)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println(err.Error())
		}

		var user_id string
		if claims, ok := authToken.Claims.(*helpers.AppClaims); ok && authToken.Valid {
			user_id = claims.UserId
		}

		var msg string
		postModel := &models.Post{
			Id:     params["id"],
			UserId: user_id,
		}

		if request.PostContent != "" {

			postModel.Post_Content = request.PostContent
			err = repository.UpdatePost(r.Context(), "posts", "post_content", postModel)
			msg = "Your post content has been Updated succesfully ..."
		}

		if request.PostTitle != "" {

			postModel.Post_Title = request.PostTitle
			err = repository.UpdatePost(r.Context(), "posts", "post_title", postModel)
			msg = "Your post title has been Updated succesfully ..."
		}

		if request.PostTitle != "" && request.PostContent != "" {
			postModel.Post_Title = request.PostTitle
			postModel.Post_Content = request.PostContent
			err = repository.UpdatePost(r.Context(), "posts", "update_all", postModel)
			msg = "Your post title and content has been Updated succesfully ..."
		}

		if err != nil {
			fmt.Println(err.Error())
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(responses.UpdatePostResponse{
			Msg: msg,
		})

	}
}

// func UpdatePostContentHandler() http.HandlerFunc {
// 	return func(w http.ResponseWriter, r *http.Request) {
// 		params := mux.Vars(r)
// 		token := r.Header.Get("Authorization")
// 		authToken, err := helpers.ParseToken(token)
//
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusUnauthorized)
// 			log.Println(err.Error())
// 		}
//
// 		var request = UpdatePostRequest{}
// 		err = json.NewDecoder(r.Body).Decode(&request)
//
// 		if err != nil {
// 			http.Error(w, err.Error(), http.StatusUnauthorized)
// 			log.Println(err.Error())
// 		}
//
// 		var model repository.Models
// 		if authToken.Valid {
// 			model, err = repository.GetPostBy(r.Context(), params["id"], "id", "posts")
// 			if err != nil {
// 				log.Println(err.Error())
// 			}
// 		}
//
// 		postModel := model.(*models.Post)
//
// 		var msg string
// 		if request.PostTitle == "" {
// 			postModel.Post_Content = request.PostContent
// 			err = repository.UpdatePost(r.Context(), "posts", "post_content", postModel)
// 			msg = "Your post content has been Updated succesfully ..."
// 		}
// 		if request.PostContent == "" {
// 			postModel.Post_Title = request.PostTitle
// 			err = repository.UpdatePost(r.Context(), "posts", "post_title", postModel)
// 			msg = "Your post Title has been Updated succesfully ..."
// 		}
// 		log.Println("PostUpdated: ", postModel)
//
// 		if err != nil {
// 			log.Println(err.Error())
// 		}
//
// 		w.Header().Set("Content-Type", "application/json")
// 		json.NewEncoder(w).Encode(UpdatePostResponse{
// 			Msg: msg,
// 		})
//
// 	}
// }
