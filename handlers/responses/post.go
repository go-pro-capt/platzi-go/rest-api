package responses

import (
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/repository"
)

type UpdatePostResponse struct {
	Msg string `json:"message"`
}

type NewPostResponse struct {
	Msg string `json:"message"`
}

type PostListResponse struct {
	UserId string         `json:"user_id"`
	Posts  *[]models.Post `json:"posts"`
}

type AllPostListResponse struct {
	Posts *[]models.Post `json:"posts"`
}

type RetrievePostResponse struct {
	Post repository.Models `json:"post"`
}
