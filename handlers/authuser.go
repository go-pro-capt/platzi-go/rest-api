package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/helpers"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/repository"
)

func AuthUserHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		header_token := r.Header.Get("Authorization")

		token, err := helpers.ParseToken(header_token)

		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			log.Println("error: ", err.Error())
			return
		}

		if claims, ok := token.Claims.(*helpers.AppClaims); ok && token.Valid {
			user, err := repository.GetUserByFlag(r.Context(), claims.UserId, "id", "users")

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Fatal(err.Error())
				return
			}

			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(user)
		} else {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}
