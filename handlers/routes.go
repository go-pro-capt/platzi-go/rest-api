package handlers

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/handlers/middlew"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/server/websockets"
)

func BindRoutes(r *mux.Router, hub *websockets.Hub) {
	r.HandleFunc("/", HomeHandler()).Methods(http.MethodGet)
	r.HandleFunc("/signup", middlew.CheckDB(SignUpHandler())).Methods(http.MethodPost)
	r.HandleFunc("/login", LoginHandler()).Methods(http.MethodPost)
	r.HandleFunc("/auth", middlew.CkeckAuthMiddlew(AuthUserHandler())).Methods(http.MethodGet)
	r.HandleFunc("/post", middlew.CkeckAuthMiddlew(CreatePostHandler(hub))).Methods(http.MethodPost)
	r.HandleFunc("/post/{id}", middlew.CkeckAuthMiddlew(GetPostByIdHandler())).Methods(http.MethodGet)
	r.HandleFunc("/post/{id}", middlew.CkeckAuthMiddlew(UpdatePostHandler())).Methods(http.MethodPut)
	r.HandleFunc("/posts/all", middlew.CkeckAuthMiddlew(GetAllPostsHandler())).Methods(http.MethodGet)
	r.HandleFunc("/posts/by_user", middlew.CkeckAuthMiddlew(GetPostsByUserHandler())).Methods(http.MethodGet)
	r.HandleFunc("/posts/by_date", middlew.CkeckAuthMiddlew(GetPostsByDateHandler())).Methods(http.MethodPost)
	// r.HandleFunc("/post/{id}", middlew.CkeckAuthMiddlew(UpdatePostContentHandler())).Methods(http.MethodPut)

	r.HandleFunc("/ws", hub.HandleSocket())
}
