package middlew

import (
	"log"
	"net/http"

	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/database/postgres"
)

var PgC, _ = postgres.GetConnection()

func CheckDB(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if postgres.CheckHealthDbConnection(PgC) == 0 {
			http.Error(w, "Lost Connection to DB", http.StatusInternalServerError)
			return
		}
		log.Println("BD Connection is Healthy ...")
		next.ServeHTTP(w, r)
	}
}
