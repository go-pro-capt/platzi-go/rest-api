package middlew

import (
	"log"
	"net/http"

	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/helpers"
)

func CkeckAuthMiddlew(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")

		_, err := helpers.ParseToken(token)

		if err != nil {
			http.Error(w, err.Error(), http.StatusUnauthorized)
			log.Fatalln(err.Error())
		}

		next.ServeHTTP(w, r)
	}
}
