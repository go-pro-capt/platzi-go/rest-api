package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/segmentio/ksuid"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/helpers"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/repository"
)

type SignUpLoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type SignUpResponse struct {
	Id    string `json:"id"`
	Email string `json:"email"`
}

type LoginResponse struct {
	Token string `json:"token"`
}

func SignUpHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var request = SignUpLoginRequest{}
		err := json.NewDecoder(r.Body).Decode(&request)

		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		id, err := ksuid.NewRandom()
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		hashedPassword, err := helpers.EncryptPassword(request.Password)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		var user = models.User{
			Id:       id.String(),
			Email:    request.Email,
			Password: hashedPassword,
		}

		err = repository.InsertUser(r.Context(), "users", &user) // DB Transaction

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		log.Printf("New User has been registered, (OK) ...") // New DBTx

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(SignUpResponse{
			Id:    user.Id,
			Email: user.Email,
		})
	}
}
