package main

import (
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/handlers"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/server"
)

func main() {
	server.Start(handlers.BindRoutes)
}
