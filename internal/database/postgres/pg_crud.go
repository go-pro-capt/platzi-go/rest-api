package postgres

import (
	"context"

	_ "github.com/lib/pq"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/database/postgres/helpers"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/repository"
)

var PgC, _ = GetConnection()

func (pgrepo *PostgresRepository) Close() error {
	return PgC.Close()
}

// Insert creates a new instance of a given model ex. a new user or a new post
func (pgrepo *PostgresRepository) Insert(ctx context.Context, db_table string, model repository.Models) error {
	pg_query := map[string]string{
		"users": "INSERT INTO users (id, email, password) VALUES ($1, $2, $3)",
		"posts": "INSERT INTO posts (id, post_title, post_content, created_at, user_id) VALUES ($1, $2, $3, $4, $5)",
	}
	// Insert user
	if db_table == "users" {
		userModel := model.(*models.User)
		if query_string, ok := pg_query[db_table]; ok {
			_, err := PgC.ExecContext(ctx, query_string, userModel.Id, userModel.Email, userModel.Password)
			return err
		}
	}
	// Insert Post
	if db_table == "posts" {
		postModel := model.(*models.Post)
		if query_string, ok := pg_query[db_table]; ok {
			_, err := PgC.ExecContext(ctx, query_string, postModel.Id, postModel.Post_Title, postModel.Post_Content, postModel.CreatedAt, postModel.UserId)
			return err
		}
	}

	return nil
}

// GetBy returns individual instance of a given model ex. a user or a post
func (pgrepo *PostgresRepository) GetBy(ctx context.Context, value string, flag string, db_table string) (repository.Models, error) {

	var u = &models.User{}
	var p = &models.Post{}

	var appModels repository.Models

	if db_table == "users" {
		user, _ := helpers.RetrieveModel(u, ctx, PgC, value, flag)
		appModels = user
		return user, nil
	}

	if db_table == "posts" {
		post, _ := helpers.RetrieveModel(p, ctx, PgC, value, flag)
		appModels = post
		return appModels, nil
	}

	return appModels, nil
}
