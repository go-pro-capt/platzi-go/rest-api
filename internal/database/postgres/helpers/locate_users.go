package helpers

import (
	"context"
	"database/sql"
	"log"

	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
)

func LocateUserBy(ctx context.Context, flag string, value string, conn *sql.DB) (*models.User, error) {
	user := &models.User{}
	switch flag {
	case "id":
		pg_query := "SELECT id, email FROM users WHERE id = $1"
		rows, err := conn.QueryContext(ctx, pg_query, value)
		if err != nil {
			return nil, err
		}
		defer func() {
			err = rows.Close()
			if err != nil {
				log.Fatal(err.Error())
			}
		}()
		user = FindUser(rows, user, "id") // User has been found By their ID
		return user, nil
	case "email":
		pg_query := "SELECT id, email, password FROM users WHERE email = $1"
		rows, err := conn.QueryContext(ctx, pg_query, value)
		if err != nil {
			return nil, err
		}
		defer func() {
			err = rows.Close()
			if err != nil {
				log.Fatal(err.Error())
			}
		}()
		user = FindUser(rows, user, flag) // User has been found by their Email
		return user, nil
	}
	return user, nil
}

func FindUser(r *sql.Rows, user *models.User, flag string) *models.User {

	var userModel = user
	if flag == "id" {
		for r.Next() {
			if err := r.Scan(&userModel.Id, &userModel.Email); err == nil {
				return userModel
			} else {
				log.Fatalln(err.Error())
			}
		}
	}
	if flag == "email" {
		for r.Next() {
			if err := r.Scan(&userModel.Id, &userModel.Email, &userModel.Password); err == nil {
				return userModel
			} else {
				log.Fatalln(err.Error())
			}
		}
	}

	return userModel
}
