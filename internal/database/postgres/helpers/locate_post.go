package helpers

import (
	"context"
	"database/sql"
	"log"
	"time"

	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
)

// LocatePostById helps to locate individual post by its Post Id
func LocatePostById(ctx context.Context, flag string, value string, conn *sql.DB) (*models.Post, error) {
	post := &models.Post{}
	switch flag {
	case "id":
		pg_query := "SELECT id, post_title, post_content, created_at, user_id FROM posts WHERE id = $1"
		rows, err := conn.QueryContext(ctx, pg_query, value)
		if err != nil {
			return nil, err
		}
		defer func() {
			err = rows.Close()
			if err != nil {
				log.Fatal(err.Error())
			}
		}()
		post = FindPost(rows, post, "id") // Post has been found By their ID
		return post, nil
	}
	return post, nil
}

// FindPost localize individual post
func FindPost(r *sql.Rows, post *models.Post, flag string) *models.Post {

	var postModel = post
	if flag == "id" {
		for r.Next() {
			if err := r.Scan(&postModel.Id, &postModel.Post_Title, &postModel.Post_Content, &postModel.CreatedAt, &postModel.UserId); err == nil {
				return postModel
			} else {
				log.Fatalln(err.Error())
			}
		}
	}

	return postModel
}

// ListPostsBy return a slice of posts
func ListPostsBy(ctx context.Context, value string, flag string, page uint64, query_string string, conn *sql.DB) (*[]models.Post, error) {
	if flag == "all_posts" {
		r, err := conn.QueryContext(ctx, query_string, 2, page*2) // Query all published posts without value
		if err != nil {
			log.Println(err.Error())
			return nil, err
		}
		defer func() {
			err = r.Close()
			if err != nil {
				log.Fatal(err.Error())
				log.Println(err.Error())
			}
		}()
		return RetrievePosts(r), nil
	}

	if flag == "by_user" {
		rows, err := conn.QueryContext(ctx, query_string, value, 2, page*2) // Query all posts given a value
		if err != nil {
			log.Println(err.Error())
			return nil, err
		}
		defer func() {
			err = rows.Close()
			if err != nil {
				log.Fatal(err.Error())
				log.Println(err.Error())
			}
		}()
		return RetrievePosts(rows), nil
	}

	if flag == "by_date" {
		start_date, err := time.Parse("2006-01-02", value)

		if err != nil {
			log.Println(err.Error())
		}
		next_date := start_date.Add(time.Hour * 24)
		r, err := conn.QueryContext(ctx, query_string, start_date, next_date, 2, page*2) // Query all published posts without value
		if err != nil {
			log.Println(err.Error())
			return nil, err
		}
		defer func() {
			err = r.Close()
			if err != nil {
				log.Fatal(err.Error())
				log.Println(err.Error())
			}
		}()
		return RetrievePosts(r), nil
	}
	return nil, nil
}

// RetrievePosts returns slice of posts from raw sql rows
func RetrievePosts(r *sql.Rows) *[]models.Post {

	var posts []models.Post
	var postModel models.Post

	for r.Next() {
		if err := r.Scan(&postModel.Id, &postModel.Post_Title, &postModel.Post_Content, &postModel.CreatedAt, &postModel.UserId); err == nil {
			posts = append(posts, postModel)
		} else {
			log.Fatalln(r)
			log.Println(err.Error())
		}
	}

	return &posts
}
