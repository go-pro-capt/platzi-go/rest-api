package helpers

import (
	"context"
	"database/sql"
	"reflect"

	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/repository"
)

func RetrieveModel(model repository.Models, ctx context.Context, conn *sql.DB, value, flag string) (repository.Models, error) {

	if reflect.TypeOf(model) == reflect.TypeOf(&models.User{}) {
		userModel, _ := LocateUserBy(ctx, flag, value, conn)
		return userModel, nil
	}
	if reflect.TypeOf(model) == reflect.TypeOf(&models.Post{}) {
		postModel, _ := LocatePostById(ctx, flag, value, conn)
		return postModel, nil
	}
	return nil, nil
}
