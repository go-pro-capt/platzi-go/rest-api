package postgres

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/database/postgres/helpers"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/repository"
)

func (pgrepo *PostgresRepository) GetPostsBy(ctx context.Context, value string, flag string, page uint64) (*[]models.Post, error) {

	pg_query := map[string]string{
		"all_posts": "SELECT id, post_title, post_content, created_at, user_id FROM posts LIMIT $1 OFFSET $2",
		"by_user":   "SELECT id, post_title, post_content, created_at, user_id FROM posts WHERE user_id = $1 LIMIT $2 OFFSET $3",
		"by_date":   "SELECT id, post_title, post_content, created_at, user_id FROM posts WHERE created_at BETWEEN $1 AND $2 LIMIT $3 OFFSET $4",
	}

	if query_string, ok := pg_query[flag]; ok {
		switch flag {
		case "all_posts": // returns all posts published by all users
			return helpers.ListPostsBy(ctx, value, flag, page, query_string, PgC)
		case "by_user": // return all posts published by a single user
			return helpers.ListPostsBy(ctx, value, flag, page, query_string, PgC)
		case "by_date": // return all posts published at a given date by all users
			return helpers.ListPostsBy(ctx, value, flag, page, query_string, PgC)
		}
	}
	return nil, nil
}

func (pgrepo *PostgresRepository) Update(ctx context.Context, db_table string, flag string, post repository.Models) error {
	pg_query := map[string]string{
		"post_title":   "UPDATE posts SET post_title = $1 WHERE id = $2 and user_id = $3",
		"post_content": "UPDATE posts SET post_content = $1 WHERE id = $2 and user_id = $3",
		"update_all":   "UPDATE posts SET post_title = $1, post_content = $2 WHERE id = $3 and user_id = $4",
	}

	if db_table == "posts" {
		postModel := post.(*models.Post)
		if query_string, ok := pg_query[flag]; ok {
			switch flag {
			case "post_title":
				_, err := PgC.ExecContext(ctx, query_string, postModel.Post_Title, postModel.Id, postModel.UserId)
				return err
			case "post_content":
				_, err := PgC.ExecContext(ctx, query_string, postModel.Post_Content, postModel.Id, postModel.UserId)
				return err
			case "update_all":
				_, err := PgC.ExecContext(ctx, query_string, postModel.Post_Title, postModel.Post_Content, postModel.Id, postModel.UserId)
				return err
			}
		}
	}
	return nil
}
