package postgres

import (
	"database/sql"
	"log"

	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/database"
)

type PostgresRepository struct{}

var repo = PostgresRepository{}

var db_url = database.BuildConfig().DatabaseUrl

var dbconn, dberr = repo.BuildConnection(db_url)

func GetRepo() *PostgresRepository {
	return &repo
}

func (pg_repo *PostgresRepository) BuildConnection(db_url string) (*sql.DB, error) {
	db, err := sql.Open("postgres", db_url)

	if err != nil {
		log.Fatal(err.Error())
		return db, nil
	}

	return db, nil
}

func GetConnection() (*sql.DB, error) {
	err := dberr

	if err != nil {
		log.Fatal(err.Error())
	}

	return dbconn, nil
}

func CheckHealthDbConnection(dbC *sql.DB) int {
	err := dbC.Ping()

	if err != nil {
		return 0
	}
	return 1
}
