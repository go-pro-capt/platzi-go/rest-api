package database

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Config struct {
	Port        string
	JWTSecret   string
	DatabaseUrl string
}

func BuildConfig() *Config {
	err := godotenv.Load(".env")
	//
	if err != nil {
		log.Fatal(err.Error())
	}

	Port := os.Getenv("PORT")
	if Port == "" {
		Port = "8080"
	}
	Jwt := os.Getenv("JWT")
	if Jwt == "" {
		Jwt = "secret"
	}
	DB_Url := os.Getenv("DB_URL")
	if DB_Url == "" {
		DB_Url = "custom"
	}

	return &Config{
		Port:        Port,
		JWTSecret:   Jwt,
		DatabaseUrl: DB_Url,
	}
}
