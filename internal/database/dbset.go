package database

import "database/sql"

type DbSet interface {
	BuildConnection(string) (*sql.DB, error)
}
