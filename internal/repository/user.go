package repository

import (
	"context"
)

type UserRepository struct {
	db_repo repo
}

var UserRepo UserRepository

func NewUserRepository[C repo](db_repo C) {
	UserRepo = UserRepository{db_repo: db_repo}
}

func InsertUser(ctx context.Context, db_table string, model Models) error {
	return UserRepo.db_repo.Insert(ctx, db_table, model)
}

func GetUserByFlag(ctx context.Context, value string, flag string, db_table string) (Models, error) {
	return UserRepo.db_repo.GetBy(ctx, value, flag, db_table)
}

func Close() {
	UserRepo.db_repo.Close()
}
