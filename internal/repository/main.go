package repository

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/database"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
)

type Repository interface {
	Insert(ctx context.Context, db_table string, model Models) error
	GetBy(ctx context.Context, value string, flag string, db_table string) (Models, error)
	Update(ctx context.Context, db_table string, flag string, model Models) error
	Close() error
	database.DbSet
	PostRepo
}

type PostRepo interface {
	GetPostsBy(ctx context.Context, value string, flag string, page uint64) (*[]models.Post, error)
}
