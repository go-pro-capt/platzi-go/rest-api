package repository

import (
	"context"

	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
)

type repo = Repository

type PostRepository struct {
	db_repo repo
}

var post_repo PostRepository

func NewPostRepository(repo repo) {
	post_repo = PostRepository{db_repo: repo}
}

func InsertPost(ctx context.Context, db_table string, post Models) error {
	return post_repo.db_repo.Insert(ctx, db_table, post)
}

func GetPostsByFlag(ctx context.Context, value string, flag string, page uint64) (*[]models.Post, error) {
	return post_repo.db_repo.GetPostsBy(ctx, value, flag, page)
}

func GetPostBy(ctx context.Context, value string, flag string, db_table string) (Models, error) {
	return post_repo.db_repo.GetBy(ctx, value, flag, db_table)
}

func UpdatePost(ctx context.Context, db_table string, flag string, post Models) error {
	return post_repo.db_repo.Update(ctx, db_table, flag, post)
}
