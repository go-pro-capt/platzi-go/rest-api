module gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS

go 1.19

require (
	github.com/golang-jwt/jwt/v4 v4.4.2
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.7
	github.com/segmentio/ksuid v1.0.4
	golang.org/x/crypto v0.3.0
)

require github.com/gorilla/websocket v1.5.0 // indirect
