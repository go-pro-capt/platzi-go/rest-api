package handlers

import (
	"log"
	"net/http"
	"strconv"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/helpers"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/models"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/repository"
)

func GetAllPosts(flag string, authToken *jwt.Token, r *http.Request, req_date string) *[]models.Post {
	pageStr := r.URL.Query().Get("page")
	var page = uint64(0)
	var err error

	if pageStr != "" {
		page, err = strconv.ParseUint(pageStr, 10, 16)
		if err != nil {
			log.Println(err.Error())
		}
	}

	log.Println("Page: ", page)

	// var request = requests.GetAllPostByDateRequest{}
	// err = json.NewDecoder(r.Body).Decode(&request)
	//
	// if err != nil {
	// 	log.Println(err.Error())
	// }
	//
	// log.Println("date: ", request.Date)
	posts := &[]models.Post{}

	if claims, ok := authToken.Claims.(*helpers.AppClaims); ok && authToken.Valid {

		switch flag {
		case "all_posts":
			posts, err = repository.GetPostsByFlag(r.Context(), "", "all_posts", page)
			if err != nil {
				log.Println(err.Error())
			}
		case "by_user":
			posts, err = repository.GetPostsByFlag(r.Context(), claims.UserId, "by_user", page)
			if err != nil {
				log.Println(err.Error())
			}
		case "by_date":
			posts, err = repository.GetPostsByFlag(r.Context(), req_date, "by_date", page)
			if err != nil {
				log.Println(err.Error())
			}
		}
	}
	return posts
}
