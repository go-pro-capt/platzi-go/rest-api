package helpers

import "golang.org/x/crypto/bcrypt"

const (
	BCRYPT_COST = 8
)

func EncryptPassword(pass string) (string, error) {
	b, err := bcrypt.GenerateFromPassword([]byte(pass), BCRYPT_COST)
	return string(b), err
}

func DecryptPasword(hpass, rpass []byte) error {
	// hpass: Hashed Password; 	rpass: Request Password
	err := bcrypt.CompareHashAndPassword(hpass, rpass)
	return err
}
