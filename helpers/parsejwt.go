package helpers

import (
	"strings"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/go-pro-capt/platzi-go/rest-api/Rest-API-WS/internal/database"
)

type AppClaims struct {
	UserId string `json:"user_id"`
	jwt.StandardClaims
}

func ParseToken(hauth string) (*jwt.Token, error) {
	// hauth: Autohorization Header
	strim := strings.TrimPrefix(hauth, "Bearer ")
	token, err := jwt.ParseWithClaims(strim, &AppClaims{}, func(t *jwt.Token) (interface{}, error) {
		return []byte(database.BuildConfig().JWTSecret), nil
	})
	return token, err
}
